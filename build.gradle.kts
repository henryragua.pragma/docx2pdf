plugins {
	java
	id("org.springframework.boot") version "2.7.11"
	id("io.spring.dependency-management") version "1.0.15.RELEASE"
}

group = "com.pragma.bmm"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	compileOnly("org.projectlombok:lombok")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	annotationProcessor("org.projectlombok:lombok")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	//implementation("org.libreoffice:libreoffice")
	implementation("org.libreoffice:ridl:7.5.3")
	implementation("org.libreoffice:juh:7.5.3")
	implementation("org.libreoffice:jurt:7.5.3")
	implementation("org.libreoffice:unoil:7.5.3")
	implementation("org.libreoffice:libreoffice:7.5.3")
	implementation("org.libreoffice:unoloader:7.5.3")


	// https://mvnrepository.com/artifact/org.libreoffice/unoil

	//implementation("org.libreoffice:bootstrap-connector:0.1.1")
	implementation("com.github.jeremysolarz:bootstrap-connector:1.0.0")

}

tasks.withType<Test> {
	useJUnitPlatform()
}
