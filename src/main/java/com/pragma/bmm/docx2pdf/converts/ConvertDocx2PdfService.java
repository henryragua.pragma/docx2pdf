package com.pragma.bmm.docx2pdf.converts;

import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XStorable;
import com.sun.star.io.IOException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import ooo.connector.BootstrapSocketConnector;
import ooo.connector.server.OOoServer;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.List;

@Service
public class ConvertDocx2PdfService {
    public void convertDocx2Pdf(String libreOfficePathParent, String docxPath, String pdfPath)
            throws java.lang.Exception {
        // Connect to OOo
        List<String> oooOptions = createParametersOfficeService();
        OOoServer oooServer = new OOoServer(libreOfficePathParent, oooOptions);
        BootstrapSocketConnector bootstrapSocketConnector = new BootstrapSocketConnector(oooServer);
        XComponentLoader xcomponentloader = getComponentLoader(bootstrapSocketConnector.connect());

        // Load the Document
        XComponent component = loadDocxComponent(docxPath, xcomponentloader);

        // save as a PDF
        savePdfFromComponent(pdfPath, component);
        //bootstrapSocketConnector.disconnect();
    }

    private void savePdfFromComponent(String pdfPath, XComponent component) throws MalformedURLException, IOException {
        XStorable xStorable = UnoRuntime
                .queryInterface(XStorable.class, component);

        PropertyValue[] propertyValues = new PropertyValue[2];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Overwrite";
        propertyValues[0].Value =true;
        propertyValues[1] = new PropertyValue();
        propertyValues[1].Name = "FilterName";
        propertyValues[1].Value = "writer_pdf_Export";

        // Appending the favoured extension to the origin document name
        String destinationUrl = new File(pdfPath).toURI().toURL().toString();
        xStorable.storeToURL(destinationUrl, propertyValues);
    }

    private XComponent loadDocxComponent(String docxPath, XComponentLoader xcomponentloader) throws FileNotFoundException, MalformedURLException, IOException {
        if (!new File(docxPath).canRead()) {
            throw new FileNotFoundException("No es posible encontrar el archivo:" + docxPath);
        }

        PropertyValue[] propertyValues = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";
        propertyValues[0].Value = true;

        String sourceUrl = new File(docxPath).toURI().toURL().toString();
        return xcomponentloader.loadComponentFromURL(sourceUrl, "_blank", 0, propertyValues);
    }

    private List<String> createParametersOfficeService() {
        return List.of(
                "--nologo",
                "--nodefault",
                "--norestore",
                "--nolockcheck"
        );
    }

    private XComponentLoader getComponentLoader(XComponentContext remoteContext) throws Exception {
        XMultiComponentFactory remoteServiceManager = remoteContext.getServiceManager();
        Object desktop = remoteServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", remoteContext);
        return UnoRuntime.queryInterface(XComponentLoader.class,desktop);
    }
}
