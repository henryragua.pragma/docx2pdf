package com.pragma.bmm.docx2pdf.controllers;

import com.pragma.bmm.docx2pdf.converts.ConvertDocx2PdfService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/docx-to-pdf")
public class Docx2PdfController {
    final ConvertDocx2PdfService convertDocx2PdfService;

    public Docx2PdfController(ConvertDocx2PdfService convertDocx2PdfService) {
        this.convertDocx2PdfService = convertDocx2PdfService;
    }
    @GetMapping
    public String convertDocx2Pdf(@RequestParam String fileName) {
        ConvertDocx2PdfService convert = new ConvertDocx2PdfService();
        try {
            convert.convertDocx2Pdf(
                    "/Applications/LibreOffice.app/Contents/MacOS/",
                    "/Users/henry.ragua/temp/" + fileName + ".docx",
                    "/Users/henry.ragua/temp/" + fileName + ".pdf"
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "ok";
    }
}
