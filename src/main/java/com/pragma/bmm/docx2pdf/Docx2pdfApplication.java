package com.pragma.bmm.docx2pdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Docx2pdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(Docx2pdfApplication.class, args);
	}

}
